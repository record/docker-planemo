ARG REGISTRY_DOMAIN="docker.io/library"

FROM python:3.9.12-slim-bullseye

LABEL maintainer "Patrick Chabrier <patrick.chabrier@inrae.fr>"

ENV DEBIAN_FRONTEND noninteractive
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PIP_NO_CACHE_DIR=1

# Singularity-ce install in 3.9.2

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y -V \ 
        build-essential \ 
        uuid-dev \ 
        libgpgme-dev \ 
        squashfs-tools \ 
        libseccomp-dev \ 
        wget \ 
        pkg-config \ 
        git \ 
        cryptsetup-bin \
    && rm -rf /var/lib/apt/lists/*


# hadolint ignore=SC2016
RUN VERSION=1.17.6 OS=linux ARCH=amd64 \
    && wget -q https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz \ 
    && tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz \ 
    && rm go$VERSION.$OS-$ARCH.tar.gz \
    && echo 'export GOPATH=${HOME}/go' >> ~/.bashrc \ 
    && echo 'export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin' >> ~/.bashrc \ 
    && . ~/.bashrc

# hadolint ignore=DL3003,SC1091
RUN export VERSION=3.9.2 \ 
    && wget -q https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz \ 
    && tar -xzf singularity-ce-${VERSION}.tar.gz \
    && (cd singularity-ce-${VERSION} \
    && . ~/.bashrc \
    && . ./mconfig \ 
    && make -C ./builddir \ 
    && make -C ./builddir install) \
    && rm -rf singularity-ce-${VERSION}

# Planemo install in 0.74.9

RUN pip install --no-cache-dir git+https://github.com/galaxyproject/planemo.git@0.74.9

# Galaxy install in 21.05

WORKDIR /galaxy-central

RUN  git clone -b release_21.05 https://github.com/galaxyproject/galaxy.git
    
WORKDIR /galaxy-central/galaxy
RUN ./run.sh start \
    && ./run.sh stop
WORKDIR /galaxy-central

# Dummy test to pre install somme dependencies

COPY tool/dummy.xml /galaxy-central/tool/dummy.xml

WORKDIR /work

RUN planemo test --galaxy_root /galaxy-central/galaxy /galaxy-central/tool/dummy.xml \
    && rm -rf tool*

EXPOSE 9090

ENTRYPOINT ["planemo"]
CMD ["--help"]
