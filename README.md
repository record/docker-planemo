# docker-planemo

A simple container hosting planemo, singularity and a galaxy intance.

# Use cases

Assuming you have docker installed:
* "Docker container list" is usefull to see what's going on.
* "Docker container stop *CONTAINER ID*" is usefull to halt a running one.

## Get the help of planemo

```
docker run registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9
```

## Get the planemo version

```
docker run registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 --version
```

## Get the singularity version

```
docker run --entrypoint="" registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 singularity --version
```

## Check the syntax of a tool

Assuming you have a tool/dummy.xml in your current working folder

```
docker run -v $PWD/tool:/galaxy-central/tool registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 \
lint tool/dummy.xml
```

## Test a tool withe the available Galaxy

Assuming :
* you have a tool/dummy.xml in your current working folder
* you want to test against the Galaxy instance already available inside the container


```
docker run -v $PWD/tool:/galaxy-central/tool registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 \
test  --galaxy_root galaxy \
tool/dummy.xml
```

## Test the tool with another Galaxy version

Assuming :
* you have a tool/dummy.xml in your current working folder
* you want to test against a specific Galaxy instance

```
docker run -v $PWD/tool:/galaxy-central/tool registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 \
test  --install_galaxy \
--galaxy_branch release_21.05 \
tool/dummy.xml
```

## Test with more feedback

Assuming :
* you have a tool/dummy.xml in your current working folder
* you want to get the log provided
* you want to see what has been generated

```
docker run --entrypoint="" \
-v /tmp:/tmp -v $PWD/tool:/galaxy-central/tool \
registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 \
bash -c "planemo test \
--no_cleanup \
--galaxy_root galaxy \
tool/dummy.xml \
&& cp tool_test* tool"
```

This way you get the log files inside your tool folder,
and you can see the code generated inside /tmp

## Test with mode feedback and with singularity

You will need to run docker with "--privileged" and
planemo with "--job_config_file tools/job_conf.xml",
where singularity is enabled.

```
docker run --privileged \
--entrypoint="" \
-v /tmp:/tmp -v $PWD/tool:/galaxy-central/tool \
registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 \
bash -c "planemo test --no_cleanup \
--job_config_file tool/job_conf.xml \
--galaxy_root galaxy  \
tool/dummy.xml \
&& cp tool_test* tool"
```

## visualize the tool inside a launched instance and play with:

```
docker run \
-p 9090:9090 \
--privileged \
--entrypoint="" \
-v /tmp:/tmp -v $PWD/tool:/galaxy-central/tool \
registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 \
bash -c "planemo serve \
--host 0.0.0.0 \
--no_cleanup \
--job_config_file tool/job_conf.xml \
--galaxy_root galaxy  \
tool/dummy.xml"
```

http://0.0.0.0:9090/ should serve galaxy an the tool tested.


## work directly inside the container with all the convenient options

```
docker run \
-it \
-p 9090:9090 \
--privileged \
--entrypoint="" \
-v /tmp:/tmp -v $PWD/tool:/galaxy-central/tool \
registry.forgemia.inra.fr/record/docker-planemo/singularity-planemo-galaxy:0.74.9 \
/bin/bash
```



